import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import React, {Component} from "react";

import './Relation.css'
import ListRelation from "./ListRelation";
import {ContextMenu, MenuItem} from "react-contextmenu";
import RelationTab from "../WorkSpace/Tabs/RelationTab";
import {pushTab} from "../../store/spaceTabs";
import ConstraintTab from "../WorkSpace/Tabs/ConstraintTab";
import RuleTab from "../WorkSpace/Tabs/RuleTab";

const RELATION_MENU_WITH_CONSTRAINT = 'RELATION_MENU_WITH_CONSTRAINT';
const RELATION_MENU_WITH_RULES = 'RELATION_MENU_WITH_RULES';
const RULE_MENU = 'RULE_MENU';
const CONSTRAINT_MENU = 'CONSTRAINT_MENU';

class Relation extends Component {

    constructor(props, context) {
        super(props, context);

        this.handleClickRelation = this.handleClickRelation.bind(this);
        this.handleClickConstraint = this.handleClickConstraint.bind(this);
        this.handleClickRule = this.handleClickRule.bind(this);
        this.getCollect = this.getCollect.bind(this);

        this.state = {
            data: undefined
        };

    }

    render() {
        const {ProjectModel, isModelLoading} = this.props;
        let modelIterator;
        let RelationsTag = null;
        if (isModelLoading || (Object.entries(ProjectModel).length === 0 && ProjectModel.constructor === Object)) {
            //modelName = "nope";
        } else {
            modelIterator = ProjectModel.firstElementChild.lastElementChild;
            if (modelIterator.childElementCount > 0) {
                RelationsTag = [...modelIterator.children].map((relationTag) => (
                    <ListRelation
                        key={relationTag.getAttribute("id")}
                        RelationTag={relationTag}
                        relation_id={relationTag.getAttribute("id")}
                        title={relationTag.getAttribute("shortName")}
                        collect={this.getCollect}
                    />
                ));
            }
        }
        return (
            <div className="relation">
                <div className="relationTitle">Отношения</div>
                {isModelLoading && <h4>Загрузка...</h4>}
                <div className="tHeader">
                    Наименование
                </div>
                <div className="relationBlock">{RelationsTag}</div>
                <ContextMenu id={RELATION_MENU_WITH_CONSTRAINT}>
                    <MenuItem onClick={this.handleClickRelation} data={{item: 'add'}}>Добавить отношение</MenuItem>
                    <MenuItem onClick={this.handleClickRelation} data={{item: 'edit'}}>Редактировать
                        отношение</MenuItem>
                    <MenuItem onClick={this.handleClickRelation} data={{item: 'delete'}}>Удалить отношение</MenuItem>
                    <MenuItem divider/>
                    <MenuItem onClick={this.handleClickRelation} data={{item: 'addConstraint'}}>Добавить
                        ограничение</MenuItem>
                </ContextMenu>
                <ContextMenu id={RELATION_MENU_WITH_RULES}>
                    <MenuItem onClick={this.handleClickRelation} data={{item: 'add'}}>Добавить отношение</MenuItem>
                    <MenuItem onClick={this.handleClickRelation} data={{item: 'edit'}}>Редактировать
                        отношение</MenuItem>
                    <MenuItem onClick={this.handleClickRelation} data={{item: 'delete'}}>Удалить отношение</MenuItem>
                    <MenuItem divider/>
                    <MenuItem onClick={this.handleClickRelation} data={{item: 'addRule'}}>Добавить правило</MenuItem>
                </ContextMenu>
                <ContextMenu id={CONSTRAINT_MENU}>
                    <MenuItem onClick={this.handleClickConstraint} data={{item: 'edit'}}>Редактировать
                        ограничение</MenuItem>
                    <MenuItem onClick={this.handleClickConstraint} data={{item: 'delete'}}>Удалить
                        ограничение</MenuItem>
                </ContextMenu>
                <ContextMenu id={RULE_MENU}>
                    <MenuItem onClick={this.handleClickRule} data={{item: 'edit'}}>Редактировать правило</MenuItem>
                    <MenuItem onClick={this.handleClickRule} data={{item: 'delete'}}>Удалить правило</MenuItem>
                </ContextMenu>
            </div>
        )
    }

    handleClickRelation(e, data, target) {
        console.log('handleClickRelation');
        console.log(this.state.data);
        if (this.state.data === undefined) return;
        switch (data.item) {
            case 'add':
                console.log('addRelation ' + 'parent ' + this.state.data.relation_id);
                break;
            case 'edit':
                console.log('editRelation ' + this.state.data.relation_id);
                this.props.pushTab({
                    title: this.state.data.title,
                    id: this.state.data.relation_id,
                    content: <RelationTab
                        idRelation={this.state.data.relation_id}
                        idParent={this.state.data.parent_id}
                    />
                });

                break;
            case 'delete':
                console.log('delete ' + data.id);
                break;
            case 'addConstraint':
                console.log('addParam ' + 'parent ' + data.id);
                break;
            case 'addRule':
                console.log('testClass ' + data.id);
                break;
        }

    }

    handleClickConstraint(e, data, target) {
        console.log('handleClickConstraint');
        console.log(this.state.data);

        switch (data.item) {
            case 'edit':
                console.log('editConstraint' + data.id);
                if (this.state.data === undefined) break;
                this.props.pushTab({
                    title: this.state.data.title,
                    id: this.state.data.constraint_id,
                    content: <ConstraintTab
                        idConstraint={this.state.data.constraint_id}
                        idParent={this.state.data.parent_id}
                    />
                });
                break;
            case 'delete':
                console.log('delete ' + data.id);
                break;
        }

    }

    handleClickRule(e, data, target) {
        console.log('handleClickRule');
        console.log(this.state.data);

        switch (data.item) {
            case 'edit':
                console.log('editRule' + data.id);
                if (this.state.data === undefined) break;
                this.props.pushTab({
                    title: this.state.data.title,
                    id: this.state.data.rule_id,
                    content: <RuleTab
                        idRule={this.state.data.rule_id}
                        idParent={this.state.data.parent_id}
                    />
                });
                break;
            case 'delete':
                console.log('delete ' + data.id);
                this.props.setModel(this.props.ProjectModel);
                this.updateTabs();
                break;
        }

    }


    getCollect(props) {
        console.log('getCollect');
        console.log(props);
        this.state.data = props;
        return props;
    }

}

const mapStateToProps = (state) => ({
    ProjectModel: state.model.XMLModel,
    isModelLoading: state.model.isLoading,
});

function mapDispatchToProps(dispatch) {

    return {pushTab: tab => dispatch(pushTab(tab))};

}

export default withRouter(
    connect(mapStateToProps, mapDispatchToProps)(Relation)
);