import React, {Component} from 'react';
import {ContextMenuTrigger} from "react-contextmenu";

const CONSTRAINT_MENU = 'CONSTRAINT_MENU';

class ListConstraint extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            id: undefined,
            title: undefined,
            data: undefined
        };

        this.getCollect = this.getCollect.bind(this);

    }

    render() {

        const {list} = this.props;

        return (
            <div className="element" title={list.getAttribute("shortName")}>
                <ContextMenuTrigger
                    id={CONSTRAINT_MENU} holdToDisplay={1000} collect={this.getCollect} key={list.getAttribute("id")}>
                    <div className="rules" id={list.getAttribute("id")}>
                        {list.getAttribute("shortName")}
                    </div>
                </ContextMenuTrigger>
            </div>
        );
    }

    getCollect(props) {
        this.props.collect({
            constraint_id: this.props.constraint_id,
            title: this.props.title,
            parent_id: this.props.parent_id
        });
        return {constraint_id: this.props.constraint_id, title: this.props.title, parent_id: this.props.parent_id};
    }

}

export default ListConstraint;