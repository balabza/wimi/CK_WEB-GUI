import React, {Component} from 'react';
import ListRule from "./ListRule";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {ContextMenuTrigger} from "react-contextmenu";
import ListConstraint from "./ListConstraint";

const RELATION_MENU_WITH_CONSTRAINT = 'RELATION_MENU_WITH_CONSTRAINT';
const RELATION_MENU_WITH_RULES = 'RELATION_MENU_WITH_RULES';

class ListRelation extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            id: undefined,
            title: undefined,
            data: undefined
        };

        this.getCollect = this.getCollect.bind(this);

    }

    componentWillMount() {
        // this.addMenu = new nw.Menu();
        // this.addMenu.append(new nw.MenuItem({
        //     label: 'doSomething',
        //     click: function() {
        //         // doSomething
        //     }
        // }));
    }

    render() {
        const {RelationTag, ProjectModel, isModelLoading} = this.props;
        let modelClasses = ProjectModel.firstElementChild.firstElementChild;
        let relationId = modelClasses.getAttribute('id');
        //console.log(ProjectModel.firstElementChild.firstElementChild);
        let rules = GetElementsByAttribute("rule", "relation", RelationTag.id, modelClasses);
        let constraints = GetElementsByAttribute("constraint", "relation", RelationTag.id, modelClasses);
        let menu = RelationTag.getAttribute("relationType") === 'constr' ? RELATION_MENU_WITH_CONSTRAINT : RELATION_MENU_WITH_RULES;
        modelClasses = null;

        if (rules != null && rules.length > 0) {
            modelClasses = rules.map((rule) => (
                <ListRule
                    key={rule.getAttribute("id")}
                    list={rule}
                    rule_id={rule.getAttribute("id")}
                    title={rule.getAttribute("shortName")}
                    collect={this.props.collect}
                    parent_id={relationId}
                />
            ));
        }

        let modelConstraints = null;

        if (constraints != null && constraints.length > 0) {
            modelConstraints = constraints.map((constraint) => (
                <ListConstraint
                    key={constraint.getAttribute("id")}
                    list={constraint}
                    constraint_id={constraint.getAttribute("id")}
                    title={constraint.getAttribute("shortName")}
                    collect={this.props.collect}
                    parent_id={relationId}
                />
            ));
        }

        return (
            <div className="element" title={RelationTag.getAttribute("shortName")}>
                < input
                    type="checkbox"
                    id={RelationTag.getAttribute("id")}
                    className="modelCheckBox"/>
                < label
                    htmlFor={RelationTag.getAttribute("id")}
                    className={"modelLabelClass"}>
                </label>
                <ContextMenuTrigger
                    renderTag='nobr' id={menu} holdToDisplay={1000} collect={this.getCollect}
                    key={RelationTag.getAttribute("id")}>
                    {RelationTag.getAttribute("shortName")}
                </ContextMenuTrigger>
                {modelClasses && <div className="modelRelation">{modelClasses}</div>}
                {modelConstraints && <div className="modelRelation">{modelConstraints}</div>}
            </div>

        );
    }

    getCollect(props) {
        this.props.collect({
            relation_id: this.props.relation_id,
            title: this.props.title,
            parent_id: this.props.parent_id
        });
        return {relation_id: this.props.relation_id, title: this.props.title, parent_id: this.props.parent_id};
    }
}



const mapStateToProps = (state) => ({
    ProjectModel: state.model.XMLModel,
    isModelLoading: state.model.isLoading,
});

Array.prototype.where = function (matcher) {
    let result = [];
    for (let i = 0; i < this.length; i++) {
        if (matcher(this[i])) {
            result.push(this[i]);
        }
    }
    return result;
};

function GetElementsByAttribute(tag, attr, attrValue, obj) {
    //Get elements and convert to array
    let elements = Array.prototype.slice.call(obj.getElementsByTagName(tag), 0);

    //Matches an element by its attribute and attribute value
    let matcher = function (el) {
        return el.getAttribute(attr) === attrValue;
    };

    return elements.where(matcher);
}

export default withRouter(connect(mapStateToProps)(ListRelation)
);