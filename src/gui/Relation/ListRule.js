import React, {Component} from 'react';
import {ContextMenuTrigger} from "react-contextmenu";

const RULE_MENU = 'RULE_MENU';

class ListRule extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            id: undefined,
            title: undefined,
            data: undefined
        };

        this.getCollect = this.getCollect.bind(this);

    }

    render() {

        const {list} = this.props;

        return (
            <div className="element" title={list.getAttribute("shortName")}>
                <ContextMenuTrigger
                    id={RULE_MENU} holdToDisplay={1000} collect={this.getCollect} key={list.getAttribute("id")}>
                    <div className="rules" id={list.getAttribute("id")}>
                        {list.getAttribute("shortName")}
                    </div>
                </ContextMenuTrigger>
            </div>
        );
    }

    getCollect(props) {
        this.props.collect({
            rule_id: this.props.rule_id,
            title: this.props.title,
            parent_id: this.props.parent_id
        });
        return {rule_id: this.props.rule_id, title: this.props.title, parent_id: this.props.parent_id};
    }

}

export default ListRule;