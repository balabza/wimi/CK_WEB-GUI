import React, {Component} from 'react'
import {Panel, PanelList, Tab, TabList, Tabs} from 'react-tabtab';
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {setIndex, setTabs} from "../../store/spaceTabs";
import {simpleSwitch} from 'react-tabtab/lib/helpers/move';
import * as customStyle from 'react-tabtab/lib/themes/bulma';
import './TabsSpace.css';

class TabsSpace extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            title: "",
            tabsTemplate: [],
            panelTemplate: [],
            activeIndex: 0,
        };
        this.style = {
            TabList: React.Element,
            Tab: React.Element,
            Panel: React.Element,
            ActionButton: React.Element
        };

        this.handleEdit = this.handleEdit.bind(this);
        this.handleTabChange = this.handleTabChange.bind(this);
        this.handleTabSequenceChange = this.handleTabSequenceChange.bind(this);
    }

    handleTabChange = (index) => {
        this.props.setIndex(index);
    };

    handleEdit = ({type, index}) => {


        if (type === 'delete') {
            this.props.setTabs([...this.props.tabArray.slice(0, index), ...this.props.tabArray.slice(index + 1)]);
        }
        if (index - 1 >= 0) {
            this.props.setIndex(index - 1);
        } else {
            this.props.setIndex(0);
        }

    };


    componentWillUpdate(nextProps, nextState, nextContext) {
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        console.log('shouldComponentUpdate');
        console.log(nextProps);
        if (nextProps.spaceActiveIndex !== this.props.spaceActiveIndex) return true;
        if (nextProps.ProjectModel !== this.props.ProjectModel) return true;
        console.log('next lenght = ' + nextProps.tabArray.length + 'old length = ' + this.props.tabArray.length);
        if (nextProps.tabArray.length !== this.props.tabArray.length) {
            return true;
        } else {
            console.log('nextProps.tabArray');
            console.log(JSON.stringify(nextProps.tabArray));
            console.log('this.props.tabArray');
            console.log(JSON.stringify(this.props.tabArray));
            return JSON.stringify(nextProps.tabArray) === JSON.stringify(this.props.tabArray);
        }
        return false;

    }

    componentWillReceiveProps(nextProps, nextContext) {
        const {tabArray, spaceActiveIndex} = nextProps;
        const tabsTemplate = [];
        const panelsTemplate = [];
        console.log(tabArray);
        if (tabArray != null || tabArray !== []) {
            tabArray.forEach((tab) => {
                tabsTemplate.push(<Tab key={'tab_' + tab.id} closable={true}>{tab.title}</Tab>);
                panelsTemplate.push(<Panel key={'panel_' + tab.id}>{tab.content}</Panel>);
            })
        }
        this.setState({tabsTemplate: tabsTemplate, panelsTemplate: panelsTemplate, activeIndex: spaceActiveIndex});
    }

    handleTabSequenceChange({oldIndex, newIndex}) {
        const {tabArray} = this.props;
        const updateTabs = simpleSwitch(tabArray, oldIndex, newIndex);
        this.props.setTabs(updateTabs);
        this.props.setIndex(newIndex);
    }

    componentWillMount() {
        const {tabArray, spaceActiveIndex} = this.props;
        const tabsTemplate = [];
        const panelsTemplate = [];
        console.log(tabArray);
        if (tabArray != null || tabArray !== []) {
            tabArray.forEach((tab, i) => {
                tabsTemplate.push(<Tab key={'tab_' + tab.id} closable={true}>{tab.title}</Tab>);
                panelsTemplate.push(<Panel key={'panel_' + tab.id}>{tab.content}</Panel>);
            })
        }
        this.setState({tabsTemplate: tabsTemplate, panelsTemplate: panelsTemplate, activeIndex: spaceActiveIndex});
    }

    render() {
        //const {tabArray, spaceActiveIndex} = this.props;
        const {tabsTemplate, panelsTemplate, activeIndex} = this.state;
        console.log("reader TabsSpace");
        // const tabsTemplate = [];
        // const panelsTemplate = [];
        // console.log(tabList);
        // if (tabList != null || tabList !== []) {
        //     tabList.forEach((tab, i) => {
        //         tabsTemplate.push(<Tab key={i} closable={true}>{tab.title}</Tab>);
        //         panelsTemplate.push(<Panel key={i}>{tab.content}</Panel>);
        //     })
        // }

        return (
            <div>
                <Tabs onTabEdit={this.handleEdit}
                      onTabChange={this.handleTabChange}
                      activeIndex={activeIndex}
                    //customStyle={this.props.customStyle}
                    //onTabSequenceChange={this.handleTabSequenceChange}
                      customStyle={customStyle}
                >
                    <TabList>
                        {tabsTemplate}
                    </TabList>
                    <PanelList>
                        {panelsTemplate}
                    </PanelList>
                </Tabs>
            </div>
        )
    }


}

const mapStateToProps = (state) => ({
    tabArray: state.spaceTabs.gTabs,
    spaceActiveIndex: state.spaceTabs.activeIndex,
    ProjectModel: state.model.XMLModel,

});

function mapDispatchToProps(dispatch) {
    return {
        setTabs: tabs => dispatch(setTabs(tabs)),
        setIndex: index => dispatch(setIndex(index))
    };

}


export default withRouter(
    connect(mapStateToProps, mapDispatchToProps)(TabsSpace)
);