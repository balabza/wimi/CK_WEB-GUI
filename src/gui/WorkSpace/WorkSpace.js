import React, {Component} from 'react'
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import TabsSpace from "./TabsSpace";
import {Header} from "semantic-ui-react";


class WorkSpace extends Component {

    componentWillUpdate(nextProps, nextState, nextContext) {
        console.log('must workspace update');
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        console.log('shouldComponentUpdate work');
        console.log(nextProps);
        if (nextProps.spaceActiveIndex !== this.props.spaceActiveIndex) return true;
        if (nextProps.ProjectModel !== this.props.ProjectModel) return true;
        console.log('next lenght = ' + nextProps.tabs.length + 'old length = ' + this.props.tabs.length);
        if (nextProps.tabs.length !== this.props.tabs.length) {
            return true;
        } else {
            console.log('nextProps.tabArray');
            console.log(JSON.stringify(nextProps.tabs));
            console.log('this.props.tabArray');
            console.log(JSON.stringify(this.props.tabs));
            return JSON.stringify(nextProps.tabs) === JSON.stringify(this.props.tabs);
        }
        return false;

    }


    updateTabs() {
        const {ProjectModel, tabs} = this.props;
        let flag = false;
        let newTabs = tabs;
        newTabs.forEach((element, index, array) => {
            let tag = ProjectModel.getElementById(element.id);
            if (tag !== undefined && tag !== null) {
                let title = tag.getAttribute('shortName');
                if (title === undefined || title === null) {
                    //console.error('no attr shortName');
                    tag.setAttribute('shortName', element.title);
                } else {
                    if (element.title !== title) {
                        element.title = title;
                        array[index] = element;
                        flag = true;

                    }
                }
            } else {
                array.splice(index, 1);
                flag = true;
                if (index > 0 && index >= array.length)
                    this.props.setIndex(index - 1);
            }
        });
        console.log(newTabs);
        console.log("push " + flag);
        if (flag) this.props.setTabs(newTabs);

    }


    render() {
        const {ProjectModel, isModelLoading, tabs} = this.props;
        console.log('render WorkSpace');
        return (
            <div className="WorkSpace">
                {!ProjectModel || (Object.entries(ProjectModel).length === 0 && ProjectModel.constructor === Object) ?
                    <Header size='medium' as='h4'>Чтобы начать загрузите XML файл.</Header> : null}
                {tabs.length > 0 && <TabsSpace/>}
            </div>
        )
    }

}

const mapStateToProps = (state) => ({
    ProjectModel: state.model.XMLModel,
    isModelLoading: state.model.isLoading,
    tabs: state.spaceTabs.gTabs,
});

export default withRouter(
    connect(mapStateToProps)(WorkSpace)
);
