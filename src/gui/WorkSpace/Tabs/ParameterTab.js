import React, {Component} from 'react'
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {pushTab} from "../../../store/spaceTabs";
import {Form, Table} from 'semantic-ui-react'
import Input from "semantic-ui-react/dist/commonjs/elements/Input";
import Header from "semantic-ui-react/dist/commonjs/elements/Header";
import TextArea from "semantic-ui-react/dist/commonjs/addons/TextArea";
import Grid from "semantic-ui-react/dist/commonjs/collections/Grid";
import './ParameterTab.css'
import {setModel} from "../../../store/model";


class ParameterTab extends Component {
    constructor(props, context) {
        super(props, context);
        this.dateTypes = [
            {
                text: "Текст",
                value: 'string'
            },
            {
                text: "Число",
                value: 'double'
            }
        ];
        this.state = {
            searchLevelString: '',
            roleFilter: null,
            parameterName: null,
            description: null,
            type: null,
            defaultValue: null,
            parameter: null,
        };
        this.handleChangeLevel = this.handleChangeLevel.bind(this);
        this.changedDataType = this.changedDataType.bind(this);
        this.setAttributes = this.setAttributes.bind(this);
        this.updateName = this.updateName.bind(this);
    }

    componentWillMount() {
        const {idParameter, ProjectModel, idParent, classesList} = this.props;
        let tagElement = ProjectModel.getElementById(idParameter);
        let parent = classesList.find((element, index, array) => {
            return element.classId === idParent;
        });
        let type = tagElement.getAttribute("type");
        let typeElement = this.dateTypes.find((element, index, array) => {
            return element.value === type;
        });
        console.log(typeElement);
        if (tagElement !== undefined || tagElement !== null)
            this.setState({
                parameterName: tagElement.getAttribute("shortName"),
                description: tagElement.getAttribute("description"),
                searchLevelString: parent.name,
                type: typeElement.value,
                defaultValue: tagElement.getAttribute("defaultValue"),
                parameter: tagElement,
            });
    }

    setAttributes(e) {
        const {parameter, parameterName} = this.state;
        const {ProjectModel, setModel} = this.props;
        if (parameter.getAttribute("shortName") !== parameterName) parameter.setAttribute('shortName', parameterName);
        console.log(ProjectModel);
        setModel(ProjectModel);
    }

    handleChangeLevel(e) {
        console.log(e.target);
        this.setAttributes();
        this.setState({searchLevelString: e.target.value});
    }

    changedDataType(e, data) {
        console.log(data);
        this.setState({type: data.value});
        this.setAttributes();
    }

    updateName(e, data) {
        console.log(data);
        this.setState({parameterName: data.value});
        const {parameter} = this.state;
        if (parameter.getAttribute("shortName") !== data.value) parameter.setAttribute('shortName', data.value);
        this.setAttributes();
    }

    updateDescription(e) {
        this.setAttributes();
        this.setState({description: e.target.value});
    }

    updateDefaultValue(e) {
        this.setAttributes();
        this.setState({defaultValue: e.target.value});
    }

    render() {
        const {idParameter, parent, idParent, classesList, isModelLoading, rules, relations, ProjectModel} = this.props;
        const {searchLevelString, roleFilter, type} = this.state;
        console.log(parent);
        console.log(ProjectModel);
        console.log(ProjectModel.getElementById(idParameter));
        //let classElement = parent.getElementById(idParameter);
        let it = 1;
        let rulesList = findTagByIDinAttribute(idParameter, rules, ['initId', 'resultId']).map((element) => (
            {
                number: it++,
                name: element.getAttribute('shortName'),
                relation: [...relations].find((element1) => {
                    return element1.getAttribute('id') === element.getAttribute('relation');
                }).getAttribute('shortName'),
            }
        ));
        let constraints = parent.getElementsByTagName('constraints');
        console.log(constraints);
        it = 1;
        let constraintList = (constraints !== null && constraints.length > 0) ? [...constraints[0].children].map((element) => (
            {
                number: it++,
                name: element.getAttribute('shortName'),
                relation: [...relations].find((element1) => {
                    return element1.getAttribute('id') === element.getAttribute('relation');
                }).getAttribute('shortName'),
            }
        )) : null;

        console.log("type: " + type);
        console.log(rulesList);
        console.log(constraintList);
        let text = classesList;
        if (searchLevelString !== null || searchLevelString !== undefined) {
            text = text.filter(info => info.name.toLowerCase().match(searchLevelString.toLowerCase()));
        }

        return (
            <div>
                <Form size='small'>
                    <Grid className="noMargin">
                        <Grid.Row verticalAlign='middle' className="noPadding">
                            <Form.Field className="noPadding" width={3}>
                                <label>Наименование</label>
                            </Form.Field>
                            <Grid.Column className="noPadding" width={13}>
                                <Input fluid type='text' placeholder="Имя параметра"
                                       value={this.state.parameterName} onChange={this.updateName}/>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row verticalAlign='middle' className="noPadding">
                            <Grid.Column className="noPadding" width={3}>
                                <Form.Field>
                                    <label>Уровень</label>
                                </Form.Field>
                            </Grid.Column>
                            <Grid.Column className="noPadding" width={13}>
                                <Input fluid list="level-select" value={searchLevelString}
                                       onChange={this.handleChangeLevel} placeholder="Класс родитель параметра"/>
                                <datalist id="level-select">
                                    {text.map(info => (
                                        <option value={info.name} key={info.classId}>{info.name}</option>
                                    ))}
                                </datalist>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row verticalAlign='middle' className="noPadding">
                            <Grid.Column className="noPadding" width={3}>
                                <Form.Field>
                                    <label>Тип данных</label>
                                </Form.Field>
                            </Grid.Column>
                            <Grid.Column className="noPadding" width={13}>
                                <Form.Select fluid id="dataTypeSelect" value={type}
                                             options={this.dateTypes}
                                             onChange={this.changedDataType}/>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row verticalAlign='middle' className="noPadding">
                            <Grid.Column className="noPadding" width={3}>
                                <Form.Field>
                                    <label>Значение</label>
                                </Form.Field>
                            </Grid.Column>
                            <Grid.Column className="noPadding" width={13}>
                                <Input fluid placeholder="Значение по умолчанию" name='defaultValue'
                                       value={this.state.defaultValue ? this.state.defaultValue : ""}
                                       onChange={this.updateDefaultValue}/>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row verticalAlign='top' className="noPadding">
                            <Grid.Column className="noPadding" width={3}>
                                <Form.Field>
                                    <label>Описание</label>
                                </Form.Field>
                            </Grid.Column>
                            <Grid.Column className="noPadding" width={13}>
                                <TextArea style={{'resize': 'none'}} label='Описание' rows={5}
                                          placeholder="Описание параметра"
                                          value={this.state.description ? this.state.description : ""}
                                          onChange={(e) => this.updateDescription(e)}/>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Form>

                <hr/>
                <div>
                    <Header as='h3'>Правила</Header>
                    <Table celled size='small'>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>№</Table.HeaderCell>
                                <Table.HeaderCell>Наименование</Table.HeaderCell>
                                <Table.HeaderCell>Отношение</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        <Table.Body>
                            {(rulesList !== null && rulesList.length > 0)
                                ? rulesList.map(element => (
                                <Table.Row key={element.name}>
                                    <Table.Cell>{element.number}</Table.Cell>
                                    <Table.Cell singleLine>{element.name}</Table.Cell>
                                    <Table.Cell singleLine>{element.relation}</Table.Cell>
                                </Table.Row>
                                ))
                                : <Table.Row>
                                    <Table.Cell colSpan='3' textAlign="center">
                                        Параметр не используется в правилах
                                    </Table.Cell>
                                </Table.Row>}
                        </Table.Body>
                    </Table>
                </div>
                <hr/>
                <div>
                    <Header as='h3'>Отношения</Header>
                    <Table celled size='small' singleLine={true}>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>№</Table.HeaderCell>
                                <Table.HeaderCell>Наименование</Table.HeaderCell>
                                <Table.HeaderCell>Отношение</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        <Table.Body>
                            {(constraintList !== null && constraintList.length > 0) ? constraintList.map(element => (
                                    <Table.Row key={element.name}>
                                        <Table.Cell>{element.number}</Table.Cell>
                                        <Table.Cell singleLine>{element.name}</Table.Cell>
                                        <Table.Cell singleLine>{element.relation}</Table.Cell>
                                    </Table.Row>
                                ))
                                :
                                <Table.Row>
                                    <Table.Cell colSpan='3' textAlign="center">
                                        Параметр не состоит в отношениях
                                    </Table.Cell>
                                </Table.Row>
                            }
                        </Table.Body>
                    </Table>
                </div>
            </div>
        );
    }
}

function findTagByIDinAttribute(id, obj, attrs) {
    let list = [];
    for (let item of obj) {
        for (let attr of attrs) {
            if (item.getAttribute(attr).indexOf(id) > -1) {
                list.push(item);
                break;
            }
        }
    }
    console.log(list);
    return list;
}

const mapStateToProps = (state) => ({
    ProjectModel: state.model.XMLModel,
    isModelLoading: state.model.isLoading,
    classesList: state.model.classesList,
    rules: state.model.rules,
    relations: state.model.relations,
});

function mapDispatchToProps(dispatch) {
    return {
        pushTab: tab => dispatch(pushTab(tab)),
        setModel: model => dispatch(setModel(model)),
    };

}

export default withRouter(
    connect(mapStateToProps, mapDispatchToProps)(ParameterTab)
);