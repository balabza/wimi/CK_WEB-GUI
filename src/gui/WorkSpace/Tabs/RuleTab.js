import React, {Component} from 'react'
import {pushTab} from "../../../store/spaceTabs";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";

class RuleTab extends Component {
    constructor(props, context) {
        super(props, context);
    }

    componentWillMount() {
        const {idRule, ProjectModel} = this.props;
        console.log("idRule = " + idRule);
        let tagElement = ProjectModel.getElementById(idRule);
        if (tagElement !== undefined || tagElement !== null)
            this.setState({
                name: tagElement.getAttribute("shortName"),
                description: tagElement.getAttribute("description")
            });
    }

    updateName(e) {
        this.setState({name: e.target.value});
    }

    updateDescription(e) {
        this.setState({description: e.target.value});
    }

    render() {
        const {idRule, ProjectModel, classesList, isModelLoading} = this.props;
        let tagElement = ProjectModel.getElementById(idRule);
        return (
            <div>
                <form>
                    <div>
                        <label htmlFor="relationName">Наименование</label>
                        <input type='text' className="form-control" placeholder="Recipe name" name='relationName'
                               value={this.state.name} onChange={(e) => this.updateName(e)}/>
                    </div>
                    <div>
                        <label htmlFor="relation">Отношение</label>
                    </div>
                    <div>
                        <label htmlFor="description">Описание</label>
                        <textarea name="description" value={this.state.description ? this.state.description : ""}
                                  onChange={(e) => this.updateDescription(e)}/>
                    </div>
                    <div>
                        <label htmlFor="inputParameters">Входные параметры</label>
                    </div>
                </form>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    ProjectModel: state.model.XMLModel,
    isModelLoading: state.model.isLoading,
    classesList: state.model.classesList,
});

function mapDispatchToProps(dispatch) {
    return {
        pushTab: tab => dispatch(pushTab(tab)),
    };
}

export default withRouter(
    connect(mapStateToProps, mapDispatchToProps)(RuleTab)
);