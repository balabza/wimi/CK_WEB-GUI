import React, {Component} from 'react'
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {pushTab} from "../../../store/spaceTabs";

class ClassTab extends Component {
    constructor(props, context) {
        super(props, context);
    }

    componentWillMount() {
        const {idClass, ProjectModel} = this.props;
        console.log("idClass = " + idClass);
        let classElement = ProjectModel.getElementById(idClass);
        if (classElement !== undefined || classElement !== null)
            this.setState({
                className: classElement.getAttribute("shortName"),
                description: classElement.getAttribute("description")
            });
    }

    updateClassName(e) {
        this.setState({className: e.target.value});
    }

    updateDescription(e) {
        this.setState({description: e.target.value});
    }

    render() {
        const {idClass, ProjectModel, parent_id, classesList, isModelLoading} = this.props;
        let classElement = ProjectModel.getElementById(idClass);
        return (
            <div>
                <form>
                    <div>
                        <label htmlFor="className">Наименование</label>
                        <input type='text' className="form-control" placeholder="Recipe name" name='className'
                               value={this.state.className} onChange={(e) => this.updateClassName(e)}/>
                    </div>
                    <div>
                        <label htmlFor="description">Описание</label>
                        <textarea name="description" value={this.state.description ? this.state.description : ""}
                                  onChange={(e) => this.updateDescription(e)}/>
                    </div>
                </form>
                <div>
                    {}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    ProjectModel: state.model.XMLModel,
    isModelLoading: state.model.isLoading,
    classesList: state.model.classesList,
});

function mapDispatchToProps(dispatch) {
    return {pushTab: tab => dispatch(pushTab(tab))};

}

export default withRouter(
    connect(mapStateToProps, mapDispatchToProps)(ClassTab)
);