import React, {Component} from 'react'
import {pushTab} from "../../../store/spaceTabs";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";

class RelationTab extends Component {
    constructor(props, context) {
        super(props, context);
    }

    componentWillMount() {
        const {idRelation, ProjectModel} = this.props;
        console.log("idRelation = " + idRelation);
        let classElement = ProjectModel.getElementById(idRelation);
        if (classElement !== undefined || classElement !== null)
            this.setState({
                name: classElement.getAttribute("shortName"),
                description: classElement.getAttribute("description")
            });
    }

    updateName(e) {
        this.setState({name: e.target.value});
    }

    updateDescription(e) {
        this.setState({description: e.target.value});
    }

    render() {
        const {idRelation, ProjectModel, classesList, isModelLoading} = this.props;
        let classElement = ProjectModel.getElementById(idRelation);
        return (
            <div>
                <form>
                    <div>
                        <label htmlFor="relationName">Наименование</label>
                        <input type='text' className="form-control" placeholder="Recipe name" name='relationName'
                               value={this.state.name} onChange={(e) => this.updateName(e)}/>
                    </div>
                    <div>
                        <label htmlFor="relationType">Тип отношения</label>
                    </div>
                    <div>
                        <label htmlFor="relationBody">Тело отношения</label>
                    </div>
                    <div>
                        <label htmlFor="description">Описание</label>
                        <textarea name="description" value={this.state.description ? this.state.description : ""}
                                  onChange={(e) => this.updateDescription(e)}/>
                    </div>
                </form>
                <div>
                    {}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    ProjectModel: state.model.XMLModel,
    isModelLoading: state.model.isLoading,
    classesList: state.model.classesList,
});

function mapDispatchToProps(dispatch) {
    return {pushTab: tab => dispatch(pushTab(tab))};

}

export default withRouter(
    connect(mapStateToProps, mapDispatchToProps)(RelationTab)
);