import React, {Component} from 'react';
import {ContextMenuTrigger} from "react-contextmenu";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {List} from "semantic-ui-react";

const PARAMETER_MENU = 'PARAMETER_MENU';

class ListParameters extends Component {

    constructor(props, context) {
        super(props, context);

        this.getCollect = this.getCollect.bind(this);

    }

    render() {
        const {key, parameterList} = this.props;
        //console.log(this.props.parent_id);
        return (
            <List.Item style={{'padding-bottom': '0'}}>
                <ContextMenuTrigger
                    id={PARAMETER_MENU} holdToDisplay={1000} collect={this.getCollect} key={key}>
                    <List.Content floated='right' style={{'width': '20%', 'margin': '0 !important'}}>
                        <List.Header textAlign='center' style={{'margin-right': '1em'}}>
                            {parameterList.getAttribute("type")}
                        </List.Header>
                    </List.Content>
                    <List.Content style={{'width': '80%', 'margin': '0 !important'}}>
                        <List.Header className='parameter'>
                            < input
                                type="checkbox"
                                id={parameterList.getAttribute("id")}
                                className="modelCheckBox"/>
                            < label
                                htmlFor={parameterList.getAttribute("id")}
                                className={"modelLabelClassLast"}>
                            </label>{parameterList.getAttribute("shortName")}
                        </List.Header>
                    </List.Content>
                </ContextMenuTrigger>
            </List.Item>
        );
    }

    getCollect(props) {
        this.props.collect({
            parameter_id: this.props.parameter_id,
            title: this.props.title,
            parent_id: this.props.parent_id,
            parent: this.props.parent,
        });
        return {
            parameter_id: this.props.parameter_id,
            title: this.props.title,
            parent_id: this.props.parent_id,
            parent: this.props.parent,
        };
    }

}

const mapStateToProps = (state) => ({
    classesList: state.model.classesList,
});

export default withRouter(
    connect(mapStateToProps)(ListParameters)
);
