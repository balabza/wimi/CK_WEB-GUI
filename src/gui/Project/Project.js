import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import ListClass from "./ListClass";
import {connect} from "react-redux";
import './Project.css'
import {ContextMenu, MenuItem} from "react-contextmenu";
import ClassTab from "../WorkSpace/Tabs/ClassTab";
import {pushTab, setIndex, setTabs} from "../../store/spaceTabs";
import {setModel} from "../../store/model";
import ParameterTab from "../WorkSpace/Tabs/ParameterTab";
import {Table} from "semantic-ui-react";
import List from "semantic-ui-react/dist/commonjs/elements/List";
import Header from "semantic-ui-react/dist/commonjs/elements/Header";

const CLASS_MENU = 'CLASS_MENU';
const PARAMETER_MENU = 'PARAMETER_MENU';

class Project extends Component {
    constructor(props, context) {
        super(props, context);

        this.handleClickClass = this.handleClickClass.bind(this);
        this.handleClickParameter = this.handleClickParameter.bind(this);
        this.getCollect = this.getCollect.bind(this);
        this.updateTabs = this.updateTabs.bind(this);

        this.state = {
            projectID: "",
            data: undefined
        };
    }

    render() {
        const {ProjectModel, isModelLoading, classesList} = this.props;
        let modelIterator;
        let modelName = null;
        let projectClasses = null;
        let projectID = null;
        if (isModelLoading || (Object.entries(ProjectModel).length === 0 && ProjectModel.constructor === Object)) {
            //modelName = "nope";
        } else {
            console.log('classesList');
            console.log(classesList);
            modelIterator = ProjectModel.firstElementChild.children[0];
            modelName = modelIterator.getAttribute("shortName");
            projectID = modelIterator.getAttribute("id");
            projectClasses = <ListClass
                key={projectID}
                classModelList={modelIterator}
                class_id={projectID}
                title={modelName}
                parent_id=""
                collect={this.getCollect}
            />;
        }
        return (
            <div className="project">
                <Header as='h2' style={{'margin': '0'}}>Проект</Header>
                <Table attached size='small' className='projectTable' style={{"height": "inherit"}}>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell textAlign='center'>Наименование</Table.HeaderCell>
                            <Table.HeaderCell textAlign='center'>Тип</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body className='tBlock'>
                        <Table.Row>
                            <Table.Cell colSpan='2' style={{'padding': '0'}}>
                                <List className='tBlock'>
                                    {projectClasses}
                                </List>
                            </Table.Cell>
                        </Table.Row>
                    </Table.Body>
                </Table>
                <ContextMenu id={CLASS_MENU}>
                    <MenuItem onClick={this.handleClickClass} data={{item: 'addClass'}}>Добавить
                        класс</MenuItem>
                    <MenuItem onClick={this.handleClickClass} data={{item: 'editClass'}}>Редактировать
                        класс</MenuItem>
                    <MenuItem onClick={this.handleClickClass} data={{item: 'delete'}}>Удалить класс</MenuItem>
                    <MenuItem divider/>
                    <MenuItem onClick={this.handleClickClass} data={{item: 'addParam'}}>Добавить
                        параметр</MenuItem>
                    <MenuItem divider/>
                    <MenuItem onClick={this.hideAll}>Свернуть все</MenuItem>
                    <MenuItem divider/>
                    <MenuItem onClick={this.handleClickClass} data={{item: 'testClass'}}>Тестировать
                        класс</MenuItem>
                </ContextMenu>
                <ContextMenu id={PARAMETER_MENU}>
                    <MenuItem onClick={this.handleClickParameter} data={{item: 'edit'}}>Редактировать
                        параметер</MenuItem>
                    <MenuItem onClick={this.handleClickParameter} data={{item: 'delete'}}>Удалить
                        параметер</MenuItem>
                </ContextMenu>

            </div>
        )
    }

    componentDidMount() {
        //console.log("test mount: " + this.G_projectID);
        //document.getElementById(this.G_projectID).style.display = "none";
    }

    componentWillUpdate(nextProps, nextState, nextContext) {

    }

    updateTabs() {
        const {ProjectModel, tabs, activeIndex} = this.props;
        let flag = false;
        let newTabs = tabs.slice();
        newTabs.forEach((element, index, array) => {
            let tag = ProjectModel.getElementById(element.id);
            if (tag !== undefined && tag !== null) {
                let title = tag.getAttribute('shortName');
                if (title === undefined || title === null) {
                    //console.error('no attr shortName');
                    tag.setAttribute('shortName', element.title);
                } else {
                    if (element.title !== title) {
                        element.title = title;
                        array[index] = element;
                        flag = true;

                    }
                }
            } else {
                array.splice(index, 1);
                if (activeIndex >= index && activeIndex !== 0)
                    this.props.setIndex(activeIndex - 1);
                flag = true;
            }
        });
        console.log(newTabs);
        console.log("push " + flag);
        if (flag) this.props.setTabs(newTabs);
        console.log(tabs);

    }

    handleClickClass(e, data, target) {
        console.log('handleClickClass ');

        console.log(this.state.data);
        switch (data.item) {
            case 'addClass':
                console.log('addClass ' + 'parent ' + data.id);
                break;
            case 'editClass':
                console.log('editClass ' + data.id);
                if (this.state.data === undefined) break;
                this.props.pushTab({
                    title: this.state.data.title,
                    id: this.state.data.class_id,
                    content: <ClassTab
                        idClass={this.state.data.class_id}
                        idParent={this.state.data.parent_id}
                    />
                });

                break;
            case 'delete':
                console.log('delete ' + data.id);
                break;
            case 'addParam':
                console.log('addParam ' + 'parent ' + data.id);
                break;
            case 'testClass':
                console.log('testClass ' + data.id);
                break;
        }

    }

    handleClickParameter(e, data, target) {
        console.log('handleClickParameter');
        if (this.state.data === undefined) return;
        console.log(this.state.data);
        switch (data.item) {
            case 'edit':
                console.log('editParameter ' + this.state.data.parameter_id);
                console.log(this.state.data.parent);
                this.props.pushTab({
                    title: this.state.data.title,
                    id: this.state.data.parameter_id,
                    content: <ParameterTab
                        key={this.state.data.parameter_id + '_ParameterTab'}
                        idParameter={this.state.data.parameter_id}
                        idParent={this.state.data.parent_id}
                        parent={this.state.data.parent}
                    />
                });
                break;
            case 'delete':
                console.log('deleteParameter ' + this.state.data.parameter_id);
                let element = this.props.ProjectModel.getElementById(this.state.data.parameter_id);
                if (element !== null) {
                    console.log('deleteParameter remove');
                    element.remove();
                    this.props.setModel(this.props.ProjectModel);
                    this.updateTabs();
                }
                break;
        }

    }

    hideAll = (e, data) => {
        const elements = [...document.getElementsByClassName("modelClass")];
        console.log("hide");
        console.log(elements);
        elements.forEach(function (children) {
            children.style.display = "none";
            console.log(children);
        })
    };

    getCollect(props) {
        console.log('getCollect');
        console.log(props);
        this.state.data = props;
        return props;
    }

}

const mapStateToProps = (store) => ({
    ProjectModel: store.model.XMLModel,
    isModelLoading: store.model.isLoading,
    classesList: store.model.classesList,
    tabs: store.spaceTabs.gTabs,
    activeIndex: store.spaceTabs.activeIndex,
});

function mapDispatchToProps(dispatch) {

    return {
        pushTab: tab => dispatch(pushTab(tab)),
        setTabs: tabs => dispatch(setTabs(tabs)),
        setIndex: index => dispatch(setIndex(index)),
        setModel: model => dispatch(setModel(model))
    };

}

export default withRouter(
    connect(mapStateToProps, mapDispatchToProps)(Project)
);