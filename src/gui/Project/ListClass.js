import React, {Component} from 'react';
import ListParameters from "./ListParameters";
import {ContextMenuTrigger} from "react-contextmenu";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import './Project.css'
import {List} from "semantic-ui-react";

const MENU_TYPE = 'CLASS_MENU';

class ListClass extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            id: undefined,
            title: undefined,
            data: undefined
        };

        this.getCollect = this.getCollect.bind(this);

    }

    render() {
        const {classModelList} = this.props;
        let modelClasses = null;

        let classID = classModelList.getAttribute("id");
        //console.log(classID);
        let iterClasses = classModelList.lastElementChild;
        if (iterClasses != null && iterClasses.childElementCount > 0) {
            //console.log(classModelList.lastElementChild.children[0]);
            modelClasses = [...classModelList.lastElementChild.children].map((classModel) => (
                <ListClass
                    key={classModel.getAttribute("id")}
                    class_id={classModel.getAttribute("id")}
                    classModelList={classModel}
                    title={classModel.getAttribute("shortName")}
                    parent_id={classID}
                    collect={this.props.collect}
                />
            ));
        }
        //console.log("parameters!");
        let parameters = getChildNodesByTag(classModelList, "parameters");
        parameters = parameters.length > 0 ? parameters[0].children : null;
        //console.log(parameters);
        let parametersShow;
        let modelName = classModelList.getAttribute("shortName");
        console.log(classModelList);
        if (parameters != null && parameters.length > 0) {
            parametersShow = [...parameters].map((parameter) => (
                <ListParameters
                    key={parameter.getAttribute("id")}
                    parameterList={parameter}
                    parameter_id={parameter.getAttribute("id")}
                    title={parameter.getAttribute("shortName")}
                    parent_id={classID}
                    collect={this.props.collect}
                    parent={classModelList}
                />
            ));
        }


        return (
            <List.Item style={{'padding-bottom': '0'}}>
                <ContextMenuTrigger
                    id={MENU_TYPE} holdToDisplay={1000} collect={this.getCollect} key={classID}>
                    <List.Content>
                        <List.Header style={{'padding-top': '0.5mm', 'padding-bottom': '0.5mm'}}>
                            < input
                                type="checkbox"
                                onChange={(e) => ListClass.change(classID, e)}
                                className="modelCheckBox"
                                id={classID + "_checkbox"}
                            />
                            < label
                                htmlFor={classID + "_checkbox"}
                                className={"modelLabelClass" + (((iterClasses && iterClasses.childElementCount) || (parameters != null && parameters.length > 0)) ? "" : "Last")}>
                            </label>
                            {modelName}
                        </List.Header>
                    </List.Content>
                    <List.Content floated='right'/>
                </ContextMenuTrigger>
                {modelClasses ?
                    <List.List id={classID} style={{
                        'display': 'none',
                        'padding-top': '0',
                        'padding-bottom': '0'
                    }}>{modelClasses}</List.List> : null}
                {parametersShow ?
                    <List.List style={{'display': 'none', 'padding-top': '0', 'padding-bottom': '0'}}
                               id={classID}>{parametersShow}</List.List> : null}
            </List.Item>
        );
    }

    static change(id, event) {
        if (id === undefined || id === null || id.length === 0 || document.getElementById(id) === null) {
            return;
        }

        if (document.getElementById(id).style.display === "none") {
            document.getElementById(id).style.display = "block";
        } else {
            if (document.getElementById(id).style.display === "block")
                document.getElementById(id).style.display = "none";
            else
                document.getElementById(id).style.display = "block"
        }

    }

    hideAll = (e, data) => {
        const elements = [...document.getElementsByClassName("modelClass")];
        console.log("hide");
        console.log(elements);
        elements.forEach(function (children) {
            children.style.display = "none";
            console.log(children);
        })
    };

    getCollect(props) {
        this.props.collect({class_id: this.props.class_id, title: this.props.title, parent_id: this.props.parent_id});
        return {class_id: this.props.class_id, title: this.props.title, parent_id: this.props.parent_id};
    }
}

function getChildNodesByTag(node, className) {
    const children = [];
    Array.from(node.children, child => {
        if (child.tagName === className) {
            children.push(child);
        }
    });
    return children;
}

const mapStateToProps = (state) => ({
    ProjectModel: state.model.XMLModel,
    isModelLoading: state.model.isLoading,
    classesList: state.model.classesList,
});

export default withRouter(
    connect(mapStateToProps)(ListClass)
);