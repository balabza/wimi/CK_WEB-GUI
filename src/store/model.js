const LOAD_REPOS_SUCCESS = 'LOAD_REPOS_SUCCESS';
const LOAD_REPOS = 'LOAD_REPOS';
const REANDER = 'REANDER';
const SET_SAVE_STATE = 'SET_SAVE_STATE';

const initialState = {
    isLoading: false,
    XMLModel: {},
    classIdList: [],
    rules: undefined,
    relations: undefined,
    isSaving: false,
};

export const loadModel = (file) => (dispatch) => {
    console.log("loading model");
    dispatch({
        type: LOAD_REPOS,
    });
    if (file) {
        let reader = new FileReader();
        reader.readAsText(file);

        reader.onload = () => {
            let parser = new DOMParser();
            let xmlRules = parser.parseFromString(reader.result, "text/xml");
            dispatch({
                type: LOAD_REPOS_SUCCESS,
                payload: xmlRules,
            })

        }
    }
};

function reducer(state = initialState, action) {
    switch (action.type) {
        case LOAD_REPOS:
            return Object.assign({}, state, {
                isLoading: true
            });
        case LOAD_REPOS_SUCCESS:
            //Project.forceUpdate();
            return Object.assign({}, state, {
                XMLModel: action.payload,
                isLoading: false,
                classesList: [...action.payload.getElementsByTagName("class")].map((classElement) => (
                    {
                        classId: classElement.getAttribute("id"),
                        name: classElement.getAttribute("shortName")
                    }
                )),
                rules: action.payload.getElementsByTagName("rule"),
                relations: action.payload.firstElementChild.lastElementChild.children,
            });
        case REANDER:
            //Project.forceUpdate();
            return Object.assign({}, state, {
                XMLModel: state.XMLModel,
            });
        case SET_SAVE_STATE:
            return Object.assign({}, state, {
                isSaving: action.payload,
            });
        default:
            return state;
    }
}

export function resetModel() {
    return {type: REANDER}
}

export function setSaveStatus(payload) {
    return {type: LOAD_REPOS_SUCCESS, payload}
}

export function setModel(payload) {
    return {type: LOAD_REPOS_SUCCESS, payload}
}

export function getModel(file, dispatch) {
    console.log("loadmodel");
    dispatch({
        type: LOAD_REPOS,
    });
    if (file) {
        let reader = new FileReader();
        reader.readAsText(file);

        reader.onload = () => {
            let parser = new DOMParser();
            let xmlRules = parser.parseFromString(reader.result, "text/xml");
            this.resultString = xmlRules.firstChild;
            console.log(this.resultString);
            let JSONRules = xmlRules.toJSON();
            console.log(xmlRules);
            dispatch({
                type: LOAD_REPOS_SUCCESS,
                payload: JSONRules
            })

        }
    }
}

export default reducer;