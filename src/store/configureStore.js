import {applyMiddleware, combineReducers, createStore} from 'redux';
import thunk from 'redux-thunk';
import model from './model';
import spaceTabs from './spaceTabs';

const store = createStore(
    combineReducers({
        model,
        spaceTabs,
    }),
    applyMiddleware(thunk)
);
export default store;