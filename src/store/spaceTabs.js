export const ADD_TAB = "ADD_TAB";
export const SET_INDEX = "SET_INDEX";
export const GET_TABS = "GET_TABS";
export const SET_TABS = "SET_TABS";
export const CLEAR_TABS = "CLEAR_TABS";

const initialState = {
    gTabs: [],
    activeIndex: 0,
};

export function pushTab(payload) {
    return {type: ADD_TAB, payload}
}

export function setIndex(payload) {
    return {type: SET_INDEX, payload}
}

export function setTabs(payload) {
    console.log('setTabs()');
    return {type: SET_TABS, payload}
}

export function clearTabs() {
    console.log('clearTabs()');
    return {type: CLEAR_TABS}
}

export function getTabs() {
    return function (dispatch) {
        return dispatch({type: GET_TABS})
    }
}

function reducer(state = initialState, action) {
    switch (action.type) {
        case ADD_TAB:
            let index = 0;
            if ((index = state.gTabs.findIndex(e => e.id === action.payload.id)) > -1) {
                return Object.assign({}, state, {
                    activeIndex: index
                });
            } else {
                return Object.assign({}, state, {
                    gTabs: state.gTabs.concat(action.payload),
                    activeIndex: state.gTabs.length
                });
            }
        case SET_INDEX:
            return Object.assign({}, state, {
                activeIndex: action.payload
            });
        case SET_TABS:
            return Object.assign({}, state, {
                gTabs: action.payload
            });
        case GET_TABS:
            return Object.assign({}, state, {
                gTabs: state.gTabs
            });
        case CLEAR_TABS:
            return Object.assign({}, state, {
                gTabs: [],
                activeIndex: 0
            });
        default:
            return state;
    }

}

export default reducer;

