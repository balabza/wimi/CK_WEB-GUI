import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {loadModel, setModel, setSaveStatus} from "../store/model";
import {clearTabs, setTabs} from "../store/spaceTabs";
import {saveAs} from 'file-saver';
import Button from "semantic-ui-react/dist/commonjs/elements/Button";
import Icon from "semantic-ui-react/dist/commonjs/elements/Icon";

class XMLFile extends Component {

    constructor(props) {
        super(props);
        this.uploadFile = this.uploadFile.bind(this);
        this.saveFile = this.saveFile.bind(this);
    }

    uploadFile(event) {
        let file = event.target.files[0];
        this.props.loadModel(file);
        this.props.clearTabs();
    }

    saveFile(e) {
        const {XMLModel, isModelLoading, isSaving} = this.props;
        setSaveStatus(true);
        console.log('XMLModel');
        console.log(XMLModel);
        let blob = new Blob([XMLModel.getElementsByTagName('model')[0].outerHTML], {type: 'text/xml'});
        setSaveStatus(false);
        saveAs(blob, "model.xml");
    }

    render() {
        const {XMLModel, isModelLoading, iSSaving} = this.props;
        return <div>
            {this.resultString}
            {isModelLoading ?
                <h3>Загрузка...</h3>
                :
                <span>
                <input type="file"
                       accept="application/xml,.xml"
                       name="file_rules"
                       onChange={this.uploadFile}/>
                </span>
            }
            {!XMLModel || (Object.entries(XMLModel).length === 0 && XMLModel.constructor === Object) ?
                null
                : <Button icon onClick={this.saveFile}>
                    <Icon name='save'/>
                </Button>}
        </div>
    }
}

const mapStateToProps = (state) => ({
    XMLModel: state.model.XMLModel,
    isModelLoading: state.model.isLoading,
    isSaving: state.model.isSaving,
});

function mapDispatchToProps(dispatch) {
    return {
        loadModel: file => dispatch(loadModel(file)),
        clearTabs: () => dispatch(clearTabs()),
        setModel: model => dispatch(setModel(model)),
        setTabs: tabs => dispatch(setTabs(tabs)),
        setSaveStatus: saveStatus => dispatch(setSaveStatus(saveStatus)),
    };
};


export default withRouter(
    connect(mapStateToProps, mapDispatchToProps)(XMLFile)
);
