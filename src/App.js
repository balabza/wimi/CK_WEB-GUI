import React, {Component} from 'react';
import './App.css';
import Rules from "./Rules/Rules";
import {BrowserRouter, withRouter} from "react-router-dom";
import Project from "./gui/Project/Project";
import {hot} from 'react-hot-loader';
import {connect} from "react-redux";
import Relation from "./gui/Relation/Relation";
import WorkSpace from "./gui/WorkSpace/WorkSpace";
import Dimmer from "semantic-ui-react/dist/commonjs/modules/Dimmer";
import Loader from "semantic-ui-react/dist/commonjs/elements/Loader";
import Segment from "semantic-ui-react/dist/commonjs/elements/Segment";

class App extends Component {
    render() {
        const {isModelLoading, isSaving} = this.props;
        return (
            <BrowserRouter>
                <div className="App">
                    <header style={{'height': '1.9cm'}}>
                        <h2 className="App-title">Добро пожаловать в Облачный КЭСМИ</h2>
                        <Rules/>
                    </header>
                    <Segment className="modelSpace">
                        <Dimmer active={isModelLoading} inverted>
                            <Loader size='massive'>Загрузка</Loader>
                        </Dimmer>
                        <Dimmer active={isSaving} inverted>
                            <Loader size='massive'>Сохранение</Loader>
                        </Dimmer>
                        <Segment className="space">
                            <Project/>
                        </Segment>
                        <Segment className="workSpace">
                            <WorkSpace/>
                        </Segment>
                        <Segment className="space">
                            <Relation/>
                        </Segment>
                    </Segment>
                </div>
            </BrowserRouter>
        );
    }
}

function mapStateToProps(store) {
    return {
        isModelLoading: store.model.isLoading,
        isSaving: store.model.isSaving,
    }
}

const mapDispatchToProps = {};

export default hot(module)(withRouter(
    connect(mapStateToProps, mapDispatchToProps)(App))
);
